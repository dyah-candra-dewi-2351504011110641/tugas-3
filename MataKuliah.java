package PEMLANKULIAH.TUGAS3;

public class MataKuliah {
    private String namaMk;
    private int kode;
    private int nilai;

    public MataKuliah(String namaMk, int kode, int nilai){
        this.namaMk = namaMk;
        this.kode = kode;
        this.nilai = nilai;
    }
    public int getKode() {
        return kode;
    }

    public void setKode(int kode) {
        this.kode = kode;
    }

    public String getNamaMk() {
        return namaMk;
    }

    public void setNamaMk(String namaMk) {
        this.namaMk = namaMk;
    }


    public void setNilai(int nilai) {
        this.nilai = nilai;
    }

    public int getNilai() {
        return nilai;
    }
    public String getNilaiHrf(){
        if (nilai >=0 && nilai <= 44){
            return "E";
        } if (nilai >44 && nilai <= 50){
            return "D";
        } if (nilai >50 && nilai <= 55) {
            return "D+";
        } if (nilai >55 && nilai <= 60) {
            return "C+";
        } if (nilai >60 && nilai <= 69) {
            return "C";
        } if (nilai >69 && nilai <= 75){
            return "B";
        } if (nilai >75 && nilai <= 80){
            return "B+";
        } if (nilai >80 && nilai <= 100) {
            return "A";
        } else {
            return "Nilai tidak valid";
        }
    }
}
