package PEMLANKULIAH.TUGAS3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner dyah = new Scanner(System.in);
        System.out.println("================================================");
        System.out.println("             APLIKASI KHS MAHASISWA");
        System.out.println("================================================");
        System.out.println("Selamat datang di Aplikasi Pengisian KHS! ");
        System.out.println("================================================");
        System.out.print("Masukkan nama : ");
        String namaMhs = dyah.nextLine();
        System.out.print("Masukkan nim : ");
        String nimMhs = dyah.nextLine();
        System.out.print("Masukkan jumlah mata kuliah : ");
        int jumlahMk = dyah.nextInt();
        System.out.println("================================================");

        Mahasiswa mahasiswaa = new Mahasiswa(namaMhs, nimMhs);
        MataKuliah[] mataKuliahh = new MataKuliah[jumlahMk];
        dyah.nextLine();

        for (int i = 0; i < jumlahMk; i++) {
            System.out.print("Nama Mata Kuliah yang ingin diinput : ");
            String namaMk = dyah.nextLine();
            System.out.print("Masukkan kode mata kuliah : ");
            int kodeMk = dyah.nextInt();
            dyah.nextLine();
            System.out.print("Masukkan nilai mata kuliah tersebut : ");
            int nilaiMk = dyah.nextInt();
            dyah.nextLine();

            mataKuliahh[i] = new MataKuliah(namaMk, kodeMk, nilaiMk);
        }
        System.out.println("================================================");
        System.out.println("                KHS MAHASISWA");
        System.out.println("================================================");
        System.out.println("Nama  : " + mahasiswaa.getNama());
        System.out.println("NIM   : " + mahasiswaa.getNim());
        System.out.println("================================================");


        for (int i = 0; i < jumlahMk; i++) {
            System.out.println("Nama Mata Kuliah\t: " + mataKuliahh[i].getNamaMk());
            System.out.println("Kode Mata Kuliah\t: " + mataKuliahh[i].getKode());
            System.out.println("Nilai Mata Kuliah\t: " + mataKuliahh[i].getNilaiHrf());
            System.out.println("================================================");

        }

    }
}
